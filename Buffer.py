import socket
import pickle
from numpy import *
'''
The idea is to build a buffer that connects across the net.
Using pickle to serialize / deserialize, in that way a lot of checks
for types don't have to occur
'''

class Server:
    def __init__(self, hostname = 'localhost', port = 1234, listenConnections = 5,
                bufferSize = 1024):
        # some inti stuff

        self.socket = socket.socket()
        self.socket.bind((hostname, port))
        self.socket.listen(listenConnections)
        self.bufferSize = bufferSize
        self.counter = 0
        self.maxBuffSize = 1024
        self.ringBuffer = [[] for i in range(self.maxBuffSize)]
    def start(self):
        ''' Start buffer and add incomming messages to ring buffer'''
        while True:
            connection, address = self.socket.accept()
            print('Got connection', address)
            # keep getting message if truncation error
            message = []
            while True:
                packet = connection.recv(self.bufferSize) # magic number? receive 10 bits
                # End of File
                if not packet:
                    break
                message.append(packet)
            self.addToBuffer(message)
            connection.close()
            # stop message kills server
            if message == 'stop server':
                break
        self.close()

    def addToBuffer(self, message):
        if self.counter > self.maxBuffSize:
            self.counter = 0
        self.ringBuffer[self.counter] = pickle.loads(b"".join(message))
        self.counter += 1

    def close(self):
        print('Closing socket')
        self.socket.close()


class Client:
    def __init__(self, hostname = 'localhost', port = 1234):
        # set up attributes of client
        [setattr(self, key, value) for key, value in locals().items() if key != 'self']
        self.socket = socket.socket()
    def sendMessage(self, message):
        message = self.encodeMessage(message)
        self.socket.connect((self.hostname, self.port))
        self.socket.send(message)
    def encodeMessage(self, message, stdEncoding = 'UTF-8'):
        print(type(message))
        return pickle.dumps(message)
    def close(self):
        self.socket.close()

if __name__== '__main__':
    s = Server()
    s.start()
